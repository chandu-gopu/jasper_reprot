/**
 * 
 */
package com.example.jasper.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.example.jasper.dao.PaySlipComponentRepo;
import com.example.jasper.dao.PaySlipRepository;
import com.example.jasper.model.EmployeeSalary;

import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRSaver;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;
import net.sf.jasperreports.view.JasperViewer;

/**
 * Licensed to the Payroll (PR) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership. The ASF licenses this file to you
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * @author Gopu Mani Chandu
 * @createdon Jan 8, 2020
 * @Package com.example.jasper.service
 * @Project jasper-test
 **/

@Service
@Slf4j
public class ReportService {

	@Autowired
	private PaySlipComponentRepo componentRepo;

	@Autowired
	private PaySlipRepository salaryRepo;

	@Autowired
	private DataSource dataSource;

	/**
	 * @return
	 * @throws JRException
	 * @throws IOException
	 * @throws SQLException
	 */
	public List<EmployeeSalary> generatePaySlips() throws JRException, IOException, SQLException {

		String method = new Throwable().getStackTrace()[0].getMethodName();

		log.info("Method Name : "+method + " Started creating the reports");
		List<EmployeeSalary> employeeSalaries = salaryRepo.findAll();

		JRPdfExporter exporter = new JRPdfExporter();
		SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
		reportConfig.setSizePageToContent(true);
		reportConfig.setForceLineBreakPolicy(false);

		SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
		exportConfig.setMetadataAuthor("Chandu");
		exportConfig.setEncrypted(true);
		exportConfig.setAllowedPermissionsHint("PRINTING");

		exporter.setConfiguration(reportConfig);
		exporter.setConfiguration(exportConfig);
		Resource reportresource = new ClassPathResource("payslip.jrxml");
		Resource imageResource = new ClassPathResource("prutech.png");
		// exporter.exportReport();
		for (EmployeeSalary employeeSalary : employeeSalaries) {

			try {
				InputStream reportStream = reportresource.getInputStream();
				InputStream logoImage = imageResource.getInputStream();
				JasperReport jasperReport = JasperCompileManager.compileReport(reportStream);
				// jasperReport.setProperty("title", employeeSalary.getEmployeeName())
				Map<String, Object> parameters = new HashMap<>();
				parameters.put("organizationId", employeeSalary.getOrganizationId());
				parameters.put("employeeNumber", employeeSalary.getEmployeeNumber());
				parameters.put("logo", logoImage);

				Map<String, BigDecimal> map = new HashMap<>();
				map.put("Basic", new BigDecimal(50000));

				parameters.put("components", map);
				JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
						dataSource.getConnection());
				exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
				exporter.setExporterOutput(
						new SimpleOutputStreamExporterOutput(employeeSalary.getEmployeeName() + "--PaySlip.pdf"));
				exporter.exportReport();
				log.info("creating the payslip for  :" + employeeSalary.getEmployeeName() + "  : Completed");
			} catch (JRException e) {
				e.printStackTrace();
			}
		}

		return employeeSalaries;
	}

	/**
	 * 
	 * @throws IOException
	 * @throws SQLException
	 * @throws JRException
	 */
	public void generatePayroll() throws IOException, SQLException, JRException {
		log.info("Started creating the reports");
		List<EmployeeSalary> employeeSalaries = salaryRepo.findAll();

		JRPdfExporter exporter = new JRPdfExporter();
		SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
		reportConfig.setSizePageToContent(true);
		reportConfig.setForceLineBreakPolicy(false);

		SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
		exportConfig.setMetadataAuthor("Chandu");
		exportConfig.setEncrypted(true);
		exportConfig.setAllowedPermissionsHint("PRINTING");

		exporter.setConfiguration(reportConfig);
		exporter.setConfiguration(exportConfig);
		Resource reportresource = new ClassPathResource("jasperReport.jrxml");
		Resource imageResource = new ClassPathResource("prutech.png");

		for (EmployeeSalary employeeSalary : employeeSalaries) {
			List<EmployeeSalary> employeeSalar = new ArrayList<>();
			employeeSalar.add(employeeSalary);
			InputStream reportStream = reportresource.getInputStream();
			InputStream logoImage = imageResource.getInputStream();
			JRBeanCollectionDataSource earnings = new JRBeanCollectionDataSource(employeeSalary.getComponents());
			JRBeanCollectionDataSource employeeBean = new JRBeanCollectionDataSource(employeeSalar);
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("Earnings", earnings);
			parameters.put("EmployeeInfo", employeeBean);
			parameters.put("logo", logoImage);
			JasperReport jasperReport = JasperCompileManager.compileReport(reportStream);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(
					new SimpleOutputStreamExporterOutput(employeeSalary.getEmployeeName() + "--PaySlip.pdf"));
			exporter.exportReport();

		}
	}
}

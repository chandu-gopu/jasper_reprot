/**
 * 
 */
package com.example.jasper.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.jasper.model.PaySlipComponent;

/**
 * Licensed to the Payroll (PR) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership. The ASF licenses this file to you
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * @author Gopu Mani Chandu
 * @createdon Jan 8, 2020
 * @Package com.example.jasper.dao
 * @Project jasper-test
 **/

@Repository
public interface PaySlipComponentRepo extends JpaRepository<PaySlipComponent, String> {

}

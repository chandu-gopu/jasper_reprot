/**
 * 
 */
package com.example.jasper.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.jasper.dao.PaySlipRepository;
import com.example.jasper.model.EmployeeSalary;
import com.example.jasper.service.ReportService;

import net.sf.jasperreports.engine.JRException;

/**
 * Licensed to the Payroll (PR) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership. The ASF licenses this file to you
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * @author Gopu Mani Chandu
 * @createdon Jan 8, 2020
 * @Package com.example.jasper.controller
 * @Project jasper-test
 **/

@Controller
public class ReportController {

	@Autowired
	private ReportService reportService;

	@GetMapping(value = "/get-employees-payslips")
	public ResponseEntity<List<EmployeeSalary>> generateEmployeeSalaries()
			throws JRException, IOException, SQLException {

		List<EmployeeSalary> paySlips = reportService.generatePaySlips();

		return new ResponseEntity<>(paySlips, HttpStatus.ACCEPTED);
	}

	@GetMapping(value = "/generate-payslips")
	public ResponseEntity<String> generatePayroll() throws JRException, IOException, SQLException {

		reportService.generatePayroll();

		return new ResponseEntity<>("Generating Payroll", HttpStatus.OK);
	}

}
